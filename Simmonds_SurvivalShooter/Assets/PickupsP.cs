﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupsP : MonoBehaviour
{
    public GameObject[] pickupPrefabs;
    public float spawnTime;
    public float spawnDelay;



    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnPickup", spawnTime, spawnDelay);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnPickup()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-24f, 24f), 0.5f, Random.Range(-24f, 24f));
        GameObject toSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        GameObject clone = Instantiate(toSpawn, randomPosition, toSpawn.transform.rotation, transform) as GameObject;
        clone.transform.localPosition = randomPosition;

    }
}
