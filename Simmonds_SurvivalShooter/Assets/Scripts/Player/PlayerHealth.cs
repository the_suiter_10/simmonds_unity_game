﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    public int foodHealth;
    public int starveTime;
    public int starveDelay;
    public int prickDelay;
    public int starveDamage;
    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    bool isDead;
    bool damaged;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
        InvokeRepeating("Starve", starveTime, starveDelay);
    }


    void Update ()
    {
        if(damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
        if(currentHealth >= 100)
        {
            currentHealth = 100;
        }
    }


    public void TakeDamage (int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickupsH"))
        {
            currentHealth += foodHealth;
            healthSlider.value = currentHealth;
        }
        if (other.gameObject.CompareTag("PickupsP"))
        {
            currentHealth = 100;
            healthSlider.value = currentHealth;
        }
        if (other.gameObject.CompareTag("Cactus"))
        {
            Invoke("Prick", prickDelay);
        }
    }
    void Starve()
    {
        currentHealth -= starveDamage;
        healthSlider.value = currentHealth;
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }
    void Prick()
    {
        currentHealth -= 10;
        healthSlider.value = currentHealth;
        playerAudio.Play();
        damageImage.color = flashColour;
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }
}
