﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{


    public static int score;
    public int level = 1;
    Text text;
    string sceneName;

    void Awake ()
    {
        text = GetComponent <Text> ();
        //score = 0;
    }

    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
    }

    void Update ()
    {
        text.text = "Score: " + score;
        if (score >= 1500 * level && sceneName == "Simmonds_Level03")
        {

        }
        else if (score >= 1500 * level)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}